#!/usr/bin/env python
# -*- coding: utf-8 -*-

#https://github.com/dgzlopes/tcp-latency
from tcp_latency import measure_latency
import statistics
import time, board, neopixel, RPi.GPIO as GPIO
import logging


def setup_led_for_latency(pixels, latency):
    if latency <= 50:
        color, color_code = "dark green", (76, 153, 0)
    
    elif latency < 100:
        color, color_code = "light green", (0, 255, 0)
    
    elif latency < 200:
        color, color_code = "yellow", (255, 255, 0)
    
    elif latency > 200:
        color, color_code = "red", (255, 0, 0)
    
    else:
        color, color_code = "red flashing? (internet is down)", (255, 0, 127)
    
    pixels.fill(color_code)
    logging.info(f"Latency, {latency}, Color, ({color})")


def setup():
    logging.basicConfig(
        filename='latency_history.log', encoding='utf-8', level=logging.DEBUG,
        format='%(asctime)s.%(msecs)03d, %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
    )
    logging.getLogger().addHandler(logging.StreamHandler())
    pixels = neopixel.NeoPixel(board.D10, 1, brightness=1, auto_write=True, pixel_order=neopixel.GRB)
    return pixels


def loop(pixels):
    while True:
        latency_list = measure_latency(host="google.com", runs=1)
        latency = statistics.mean(latency_list)
        setup_led_for_latency(pixels, latency)


try:
    pixels = setup()
    loop(pixels)
except KeyboardInterrupt:
    #Apaga todos os leds
    pixels.fill((0,0,0))
    GPIO.cleanup()


# import speedtest
# # print(speedtest.Speedtest.shell())
# test  = speedtest.Speedtest()
# print("Wifi Download Speed is ", test.download() / 1000.0 / 1000.0, "MB/s")
# print("Wifi Upload Speed is ", test.upload())

# results_dict = test.results.dict()
# print(results_dict)
# print("Wifi PING is ", results_dict.ping, "ms")
