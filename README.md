## How to run
- Create env: `python -m venv venv`
- Activate env: `source venv/bin/activate` or `source venv/bin/activate.fish` (if using Fish shell)
- Install dependencies: `pip install -r requirements.txt`
- Run: `./main.py`

## Future works
https://pythonprogramming.org/monitor-your-internet-with-python/